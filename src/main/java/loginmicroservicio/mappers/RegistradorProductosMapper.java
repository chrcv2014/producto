package loginmicroservicio.mappers;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;
import loginmicroservicio.models.RegistradorProductos;

public class RegistradorProductosMapper implements RowMapper<RegistradorProductos>{

        @Override
    public RegistradorProductos mapRow(ResultSet rs, int rowNum) throws SQLException {
   
        RegistradorProductos registradorProductos = new RegistradorProductos();
        registradorProductos.setRegistradorID(rs.getLong("registradorID"));
        registradorProductos.setUsername(rs.getString("username"));
        registradorProductos.setPassword(rs.getString("contrasena"));
        registradorProductos.setNombreCompleto(rs.getString("nombreCompleto"));
        registradorProductos.setCorreo(rs.getString("correo"));        
        return registradorProductos;
    }
}
