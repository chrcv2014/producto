package loginmicroservicio.mappers;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import loginmicroservicio.models.Cliente;

public class ClienteMapper implements RowMapper<Cliente> {
    
    @Override
    public Cliente mapRow(ResultSet rs, int rowNum) throws SQLException {
   
        Cliente cliente = new Cliente();
        cliente.setClienteID(rs.getLong("clientID"));
        cliente.setUsername(rs.getString("username"));
        cliente.setPassword(rs.getString("contrasena"));
        cliente.setNombreCompleto(rs.getString("nombreCompleto"));
        cliente.setCorreo(rs.getString("correo"));    
        cliente.setPuntosLealtad(rs.getInt("puntosLealtad"));    
        return cliente;
    }
}
