package loginmicroservicio.repository;

import java.util.List;

import org.springframework.boot.autoconfigure.data.web.SpringDataWebProperties.Pageable;

import loginmicroservicio.models.Cliente;


public class ClienteRepositoryInterface extends BaseRepositorio<Cliente> {
    public boolean create(Cliente cliente);
    public boolean update(Cliente cliente);
    public boolean hasExpired(Cliente cliente);

    public List<Insumo> read(Pageable pageable); 
}
