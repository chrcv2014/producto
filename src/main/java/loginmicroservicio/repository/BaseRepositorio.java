package loginmicroservicio.repository;

import java.util.List;
import org.springframework.boot.autoconfigure.data.web.SpringDataWebProperties.Pageable;

public interface BaseRepositorio<T> {
    public boolean create(T object);
    public boolean update(T object);
    public List<T> read(Pageable pageable);
    public T readById(int Id);
}
