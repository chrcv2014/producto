package loginmicroservicio.models;

public class RegistradorProductos {
    private long registradorID;
    private String username;
    private String password;
    private String nombreCompleto;
    private String correo;

    public RegistradorProductos(long registradorID, String username, String password, String nombreCompleto, String correo) {
        this.registradorID = registradorID;
        this.username = username;
        this.password = password;
        this.nombreCompleto = nombreCompleto;
        this.correo = correo;
    }

    public long getRegistradorID() {
        return this.registradorID;
    }

    public void setRegistradorID(long registradorID) {
        this.registradorID = registradorID;
    }

    public String getUsername() {
        return this.username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return this.password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getNombreCompleto() {
        return this.nombreCompleto;
    }

    public void setNombreCompleto(String nombreCompleto) {
        this.nombreCompleto = nombreCompleto;
    }

    public String getCorreo() {
        return this.correo;
    }

    public void setCorreo(String correo) {
        this.correo = correo;
    }

    public RegistradorProductos() {
    }

}
