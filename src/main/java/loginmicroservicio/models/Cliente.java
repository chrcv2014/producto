package loginmicroservicio.models;

public class Cliente {
    private long clienteID;
    private String username;
    private String password;
    private String nombreCompleto;
    private String correo;
    private int puntosLealtad = 0;


    public Cliente(long clienteID, String username, String password, String nombreCompleto, String correo, int puntosLealtad) {
        this.clienteID = clienteID;
        this.username = username;
        this.password = password;
        this.nombreCompleto = nombreCompleto;
        this.correo = correo;
        this.puntosLealtad = puntosLealtad;
    }

    public Cliente() {
    }

    public long getClienteID() {
        return this.clienteID;
    }

    public void setClienteID(long clienteID) {
        this.clienteID = clienteID;
    }

    public String getUsername() {
        return this.username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return this.password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getNombreCompleto() {
        return this.nombreCompleto;
    }

    public void setNombreCompleto(String nombreCompleto) {
        this.nombreCompleto = nombreCompleto;
    }

    public String getCorreo() {
        return this.correo;
    }

    public void setCorreo(String correo) {
        this.correo = correo;
    }

    public int getPuntosLealtad() {
        return this.puntosLealtad;
    }

    public void setPuntosLealtad(int puntosLealtad) {
        this.puntosLealtad = puntosLealtad;
    }

}
